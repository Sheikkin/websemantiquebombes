import java.io.InputStream;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.util.FileManager;
import org.apache.jena.vocabulary.VCARD;

	/** Tutorial 1 creating a simple model
	 */

	public class TP1 {
		
		public static void main(String[] args) {
		Model model = ModelFactory.createDefaultModel();
		model.read("websem.ttl");
        
        model.write(System.out,"turtle");
           String queryString =
                   "PREFIX dbo: <http://dbpedia.org/ontology/>\n"+
                   "PREFIX dbp: <http://dbpedia.org/property/>\n"+
                   "SELECT ?name\n" + 
                           "WHERE {\n" + 
                           "  ?a dbp:origin ?country;\n" + 
                           "  dbo:name ?name;\n" + 
                           "  FILTER (?country =\"France\")\n" + 
                           "}";

            Query query = QueryFactory.create(queryString);

            System.out.println("----------------------");

            System.out.println("Query Result Sheet");

            System.out.println("----------------------");
            
            String queryString2 =
                    "PREFIX dbp: <http://dbpedia.org/property/>\n"+
                    "SELECT ?country\n" + 
                    "WHERE {\n" + 
                    "  ?a dbp:origin ?country\n" +
                    "}";

             Query query2 = QueryFactory.create(queryString2);

             System.out.println("----------------------");

             System.out.println("Query Result Sheet");

             System.out.println("----------------------");

           
           
            QueryExecution qe = QueryExecutionFactory.create(query, model);
            ResultSet results =  qe.execSelect();
            ResultSetFormatter.out(System.out, results, query);
               
            
            QueryExecution qe2 = QueryExecutionFactory.create(query2, model);
            ResultSet results2 =  qe2.execSelect();
            ResultSetFormatter.out(System.out, results2, query2);

            qe.close();
		}
	}

